package com.mojang.mojam.sound;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.SlickException;

import java.util.HashMap;

public enum Sound {

    ALIEN_DEATH("ai_death.wav"),
    ALIEN_TALK("ai_voice.wav"),
    ALIEN_SHOOT("alienshot.wav"),
    ALIEN_SUICIDE("alien_suicide.wav"),
    ALIEN_WARNING("alien_warning.wav"),
    JETPACK_FAIL("jetpack_fail.wav"),
    BIG_EXP("big_explosion.wav"),
    PLAYER_ACC("player_accelerating.wav"),
    PLAYER_DEATH("player_death.wav"),
    PLAYER_HIT("player_gethit.wav"),
    PLAYER_JETPACK("player_jetpack.wav"),
    PLAYER_LAND("cheesy_land2.wav"),
    POWERUP1("powerup_1.wav"),
    POWERUP2("powerup_2.wav"),
    SHOT("shot.wav"),
    SHOT2("shot2.wav"),
    SHOT_LAND("shot_land.wav"),
    SLICE_APPROACHING("slice_approaching.wav"),
    SLICE_DOCKING("slice_docking.wav"),
    START_LEVEL("start_level.wav"),
    BEAM("beam.wav"),
    PICKUP_COIN("coin.wav"),
    PICKUP_HEALTH("health.wav"),
    BASE_TAKES_DAMAGE("basedamage.wav"),
    BASE_SHOT("baseshot.wav"),
    PIZZA_PLOP("pizzaplop.wav"),
    UPGRADE("upgrade.wav"),
    SPIDER_TALK("spider_talk.wav");

    private static HashMap<Sound, org.newdawn.slick.Sound> sounds = new HashMap<>();
    private static Vector3f listenerPosition = new Vector3f();
    protected String location;

    Sound(String location) {
        this.location = "sound/" + location;
    }

    public static void setListenerPosition(float x, float y, float z) {
        listenerPosition.x = x;
        listenerPosition.y = y;
        listenerPosition.z = z;
    }

    public static void playSound(Sound sound, float x, float y, float z) {
        playSound(sound, x, y, z, 1.0f, 1.0f);
    }

    public static void playSound(Sound sound, float x, float y, float z, float pitch, float volume) {
        org.newdawn.slick.Sound ssound = sounds.get(sound);
        if (ssound == null) {
            try {
                ssound = new org.newdawn.slick.Sound(sound.location);
            } catch (SlickException e) {
                System.err.println("Error, unable to load sound \"" + sound.location + "\"");
                return;
            }
            sounds.put(sound, ssound);
        }
        //        float dx = listenerPosition.x - x;
        //        float dy = listenerPosition.y - y;
        //        float dz = listenerPosition.z - z;
        //        float dist = (float) Math.sqrt((double) (dx * dx + dy * dy + dz * dz));
        ssound.play(pitch, volume);
    }

}
