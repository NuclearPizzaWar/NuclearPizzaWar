/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mojang.mojam;

import com.mojang.mojam.state.State;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Johan
 */
public class GameStateController extends StateBasedGame {

    public GameStateController() {
        super("Nuclear Pizza War");
    }

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
        for (State state : MainClass.getStateService().getStates()) {
            addState(state);
        }
    }
}
