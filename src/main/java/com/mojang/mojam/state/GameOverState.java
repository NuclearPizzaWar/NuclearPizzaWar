package com.mojang.mojam.state;

import com.mojang.mojam.MainClass;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class GameOverState extends State {

    private Image failImage;
    private Image winImage;
    private boolean isVictory;

    @Override
    public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
        failImage = new Image("GUI/game_over.png");
        winImage = new Image("GUI/victory.png");
    }

    @Override
    public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {

        g.setColor(Color.black);
        g.fillRect(0, 0, container.getWidth(), container.getHeight());

        //        g.setColor(Color.white);
        //        Gui.renderCenterString(g, "GAME OVER", container.getWidth() / 2, container.getHeight() / 2 - 100);
        if (isVictory) {
            winImage.draw(0, 0);
        } else {
            failImage.draw(0, 0);
        }
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int deltaMS) throws SlickException {

        Input input = gc.getInput();
        if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
            //            Rectangle buttonRect = getStartGameButtonRect(gc);
            //            if(buttonRect.contains(input.getMouseX(), input.getMouseY())) {
            sbg.enterState(MainClass.getStateService().getStateByType(GameState.class).getID(), new FadeOutTransition(),
                    new FadeInTransition());
            //            }
        }

    }

    public void setVictory(boolean victory) {
        isVictory = victory;
    }

}
