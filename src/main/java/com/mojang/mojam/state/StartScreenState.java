/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mojang.mojam.state;

import com.mojang.mojam.Camera;
import com.mojang.mojam.MainClass;
import com.mojang.mojam.world.Starfield;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 *
 * @author Johan
 */
public class StartScreenState extends State {

    Image splashImage;
    Image startButtonImage;
    private Starfield starfield;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {

    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        splashImage = new Image("GUI/splash.png");
        startButtonImage = new Image("GUI/button_start.png");
        starfield = new Starfield(800, 600);
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics grphcs) throws SlickException {
        Camera cam = new Camera();
        starfield.render(gc, grphcs, cam);
        grphcs.drawImage(splashImage, 0, 0);
        Rectangle buttonRect = getStartGameButtonRect(gc);
        grphcs.drawImage(startButtonImage, buttonRect.getX(), buttonRect.getY());
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int deltaMS) throws SlickException {
        Input input = gc.getInput();
        if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
            Rectangle buttonRect = getStartGameButtonRect(gc);
            if (buttonRect.contains(input.getMouseX(), input.getMouseY())) {
                sbg.enterState(MainClass.getStateService().getStateByType(GameState.class).getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        }
        starfield.update(gc, deltaMS);
    }

    private Rectangle getStartGameButtonRect(GameContainer slickContainer) {
        int boxWidth = 194;
        int boxHeight = 59;
        return new Rectangle(slickContainer.getWidth() / 2 - boxWidth / 2, slickContainer.getHeight() - boxHeight - 80,
                boxWidth, boxHeight);
    }
}
