package com.mojang.mojam.state;

import com.mojang.mojam.Camera;
import com.mojang.mojam.MainClass;
import com.mojang.mojam.world.PizzaWorld;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Music;
import org.newdawn.slick.MusicListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.util.Random;

public class GameState extends State implements MusicListener {

    private final int updatesPerSecond = 40;
    private final int msPerUpdate = 1000 / updatesPerSecond;
    private final float musicVolume = 0.6f;
    public String[] soundPaths = new String[]{
            "music/mus_fast02.ogg",
            //        "music/recording2.ogg",
            //        "music/recording3.ogg",
            //        "music/recording4.ogg",
            //        "music/recording5.ogg"
    };
    Random random = new Random();
    Music music;
    private PizzaWorld pizzaWorld;
    private Camera camera;

    void startGame(GameContainer container) throws SlickException {
        camera = new Camera();
        pizzaWorld = new PizzaWorld(camera);
        pizzaWorld.init(container);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        if (pizzaWorld != null) {
            pizzaWorld.render(container, g);
        }
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        super.enter(container, game);
        startGame(container);
        music = new Music(soundPaths[random.nextInt(soundPaths.length)]);
        music.play(1.0f, musicVolume);
        music.addListener(this);
    }

    @Override
    public void leave(GameContainer container, StateBasedGame game) {
        music.fade(500, 0.0f, true);
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        container.setMinimumLogicUpdateInterval(msPerUpdate);
        container.setMaximumLogicUpdateInterval(msPerUpdate);
    }

    // We are using a fixed update rate
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (pizzaWorld != null) {
            camera.update(delta);
            pizzaWorld.update(container, delta);

            if (pizzaWorld.isGameOver()) {
                GameOverState state = (GameOverState) MainClass.getStateService().getStateByType(GameState.class);
                state.setVictory(pizzaWorld.isVictory());
                game.enterState(MainClass.getStateService().getStateByType(GameOverState.class).getID(),
                        new FadeOutTransition(Color.black, 1600), new FadeInTransition());
            }
        }
    }

    @Override
    public void musicEnded(Music music) {
        try {
            music = new Music(soundPaths[random.nextInt(soundPaths.length)]);
            music.play(1.0f, musicVolume);
            music.addListener(this);
        } catch (Exception e) {

        }
    }

    @Override
    public void musicSwapped(Music music, Music music1) {

    }
}
