package com.mojang.mojam.state;

import com.mojang.mojam.MainClass;
import org.newdawn.slick.state.BasicGameState;

public abstract class State extends BasicGameState {

    private final int id;

    public State() {
        this.id = MainClass.getStateService().getNextId();
    }

    @Override
    public int getID() {
        return id;
    }
}
