package com.mojang.mojam.state;

import com.google.common.collect.Sets;

import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class StateService {

    private Set<State> states = Sets.newHashSet();
    private AtomicInteger intGen = new AtomicInteger();

    public int getNextId() {
        return intGen.incrementAndGet();
    }

    public void registerState(State state) {
        if (states.contains(state)) {
            throw new UnsupportedOperationException("You cannot register a state more than once!");
        }
        states.add(state);
    }

    public State getStateByType(Class stateClass) {
        for (State s : states) {
            if (s.getClass().equals(stateClass)) {
                return s;
            }
        }
        throw new UnsupportedOperationException("No state is registered with that class!");
    }

    public Set<State> getStates() {
        return states;
    }
}
