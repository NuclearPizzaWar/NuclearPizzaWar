package com.mojang.mojam.gui.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class PauseMenu extends Menu {

    private static final Color shade = new Color(0, 0, 0, .5f);

    @Override
    public void update(GameContainer container, int deltaMS) {

    }

    @Override
    public void render(GameContainer container, Graphics g) {
        if (!isVisible()) {
            return;
        }

        g.setColor(shade);
        g.fillRect(0, 0, container.getWidth(), container.getHeight());
    }

    @Override
    public int getEnableKey() {
        return Input.KEY_P;
    }
}
