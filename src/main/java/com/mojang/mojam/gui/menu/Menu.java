package com.mojang.mojam.gui.menu;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public abstract class Menu {

    private boolean visible;

    public abstract void update(GameContainer container, int deltaMS);

    public abstract void render(GameContainer container, Graphics g);

    public abstract int getEnableKey();

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
