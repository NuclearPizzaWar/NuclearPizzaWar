package com.mojang.mojam;

import com.mojang.mojam.state.GameOverState;
import com.mojang.mojam.state.GameState;
import com.mojang.mojam.state.LogoState;
import com.mojang.mojam.state.StartScreenState;
import com.mojang.mojam.state.StateService;
import org.newdawn.slick.AppGameContainer;

public class MainClass {

    private static AppGameContainer gameContainer;
    private static StateService stateService;

    public static void main(String[] args) throws Exception {
        System.out.println("I'm a little teapot. That is all.");

        stateService = new StateService();
        initStates();

        gameContainer = new AppGameContainer(new GameStateController());
        gameContainer.setDisplayMode(800, 600, false);
        gameContainer.setShowFPS(false);
        gameContainer.start();
    }

    private static void initStates() {
        getStateService().registerState(new LogoState());
        getStateService().registerState(new StartScreenState());
        getStateService().registerState(new GameState());
        getStateService().registerState(new GameOverState());
    }

    public static AppGameContainer getGameContainer() {
        return gameContainer;
    }

    public static StateService getStateService() {
        return stateService;
    }
}
