package com.mojang.mojam.util;

import java.io.File;

public enum OperatingSystem {
    WINDOWS,
    MACOS,
    SOLARIS,
    LINUX,
    UNKNOWN;

    public static OperatingSystem getPlatform() {
        final String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            return WINDOWS;
        }
        if (osName.contains("mac")) {
            return MACOS;
        }
        if (osName.contains("solaris")) {
            return SOLARIS;
        }
        if (osName.contains("linux")) {
            return LINUX;
        }
        if (osName.contains("unix")) {
            return LINUX;
        }
        return UNKNOWN;
    }

    public static File getWorkingDirectory() {
        final String userHome = System.getProperty("user.home", ".");
        File workingDirectory;
        switch (getPlatform()) {
            case LINUX:
            case SOLARIS:
                workingDirectory = new File(userHome, ".nuclearpizzawar/");
                break;
            case WINDOWS:
                final String applicationData = System.getenv("APPDATA");
                final String folder = (applicationData != null) ? applicationData : userHome;
                workingDirectory = new File(folder, ".nuclearpizzawar/");
                break;
            case MACOS:
                workingDirectory = new File(userHome, "Library/Application Support/nuclearpizzawar");
                break;
            default:
                workingDirectory = new File(userHome, "nuclearpizzawar/");
                break;
        }
        return workingDirectory;
    }
}
